﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Windows.Controls;
using System.Net;
using System.IO;
using System.Threading;
using System.Data.SQLite;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.Data;

namespace myProxy
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        
        private int page = 40;
        private string keyword = "proxylist";
        private int processCount = 10;
        
        private string dbSource = "ProxyListDB.sqlite";

        private string checkeds = "";
        private Boolean httpbuchecked = false;
        private Thread Check_T;
        private Thread Start_Proxy;
        private const string DBNAME = "ProxyListDB.sqlite";
        private const string DBTALBE = "ProxyList";
        private bool dbFile_Available = false;
        private SQLiteConnection dbConn;
        private SQLiteCommand dbCmd;
        private long total_count = 0;
        private int listviewcount = 0;
        bool saveasdb = false;
        int id = 0;
        private bool Blocked = false;
        private bool PageIsTooLarge = false;


        public MainWindow()
        {
            InitializeComponent();
        }

        /*
         * 搜索按钮部分
         */
        //启动搜索按钮
        private void OnClickStartButton(object sender, RoutedEventArgs e)
        {
            listview1.Items.Clear();
            Blocked = false;
            PageIsTooLarge = false;
            Total_Count.Text = "0";
            if (Export_Checkbox.IsChecked == true)
            {
                Create_DB_File();
            }
            startButton.IsEnabled = false;
            cancelButton.IsEnabled = true;
            CheckButton.IsEnabled = false;
            httpbuchecked = (Boolean)httpbutton.IsChecked;

            Start_Proxy = new Thread(new ThreadStart(StartButton));
            Start_Proxy.Start();
        }

        private void StartButton()
        {
            if (httpbuchecked)
            {
                List<ManualResetEvent> mrelist = new List<ManualResetEvent>();
                id = 0;
                ThreadPool.SetMaxThreads(processCount, processCount);
                while (id < page)
                {
                    if (Blocked)
                    {
                        MessageBox.Show("被Google封锁了！过一个小时再进行搜索");

                        this.startButton.Dispatcher.Invoke(
                            new Action(
                                delegate
                                {
                                    startButton.IsEnabled = true;
                                }
                                ));
                        this.CheckButton.Dispatcher.Invoke(
                        new Action(
                            delegate
                            {
                                CheckButton.IsEnabled = true;
                            }
                            ));
                        this.cancelButton.Dispatcher.Invoke(
                            new Action(
                                delegate
                                {
                                    cancelButton.IsEnabled = false;
                                }
                                ));
                        Thread.CurrentThread.Abort();
                        break;
                    }
                    else if (PageIsTooLarge)
                        break;
                    else
                    {
                        ManualResetEvent mre = new ManualResetEvent(false);
                        ThreadPool.QueueUserWorkItem(StartgProcess, new GoogleS(new Process(), 1, keyword, id, id, "ProxyFunction_GoogleS.exe", id.ToString(), mre));
                        mrelist.Add(mre);
                        id++;
                        Thread.Sleep(7000);
                    }
                }
                ManualResetEvent.WaitAll(mrelist.ToArray());

            }
            else
            {
                List<ManualResetEvent> mrel = new List<ManualResetEvent>();
                for (int j = 0; j < checkeds.Length; j++)
                {
                    ManualResetEvent mre = new ManualResetEvent(false);
                    switch (checkeds[j])
                    {
                        case ('1'): new Thread(new ParameterizedThreadStart(StarturlProcess)).Start(new GoogleS(new Process(), 1, keyword, 0, 0, "ProxyFunction_SOFTBZ.exe", "1", mre)); break;
                        case ('2'): new Thread(new ParameterizedThreadStart(StarturlProcess)).Start(new GoogleS(new Process(), 1, keyword, 0, 0, "ProxyFunction_ITMOPS.exe", "2", mre)); break;
                        case ('3'): new Thread(new ParameterizedThreadStart(StarturlProcess)).Start(new GoogleS(new Process(), 1, keyword, 0, 0, "ProxyFunction_VZLOM.exe", "3", mre)); break;
                        case ('4'): new Thread(new ParameterizedThreadStart(StarturlProcess)).Start(new GoogleS(new Process(), 1, keyword, 0, 0, "ProxyFunction_PrimeSpeed.exe", "4", mre)); break;
                        default: break;
                    }
                    mrel.Add(mre);
                }
                if(mrel.Count!=0)
                ManualResetEvent.WaitAll(mrel.ToArray());
            }


            if (saveasdb == true)
            {
                if (dbFile_Available == false)
                    Create_DB_File();
                SQLiteTransaction tran = dbConn.BeginTransaction();
                dbCmd.Transaction = tran;
                for (int i = 0; i < this.listview1.Items.Count; i++)
                {
                    myData md = listview1.Items[i] as myData;
                    try
                    {
                        dbCmd.CommandText = "insert into " + DBTALBE + " values(@ip,@port,@type,@available)";
                        dbCmd.Parameters.AddWithValue("@ip", md.ip);
                        dbCmd.Parameters.AddWithValue("@port", md.port);
                        dbCmd.Parameters.AddWithValue("@type", md.type);
                        dbCmd.Parameters.AddWithValue("@available", md.available);
                        dbCmd.ExecuteNonQuery();

                    }
                    catch (SQLiteException)
                    {
                        this.listview1.Dispatcher.Invoke(
                            new Action(
                                delegate
                                {
                                    listview1.Items.RemoveAt(i);
                                }
                        )
                        );
                        i--;
                    }
                }
                tran.Commit();
            }
           
            this.startButton.Dispatcher.Invoke(
            new Action(
                delegate
                {
                    startButton.IsEnabled = true;
                }
                )
         );
            this.CheckButton.Dispatcher.Invoke(
            new Action(
                delegate
                {
                    CheckButton.IsEnabled = true;
                }
                )
         );
            this.listview1.Dispatcher.Invoke(
                new Action(
                    delegate
                    {
                        total_count = listview1.Items.Count;
                    }
        )
        );
            this.Total_Count.Dispatcher.Invoke(
                new Action(
                    delegate
                    {
                        Total_Count.Text = total_count.ToString();
                    }
            )
            );
        }
        

      
        //使用Google方式进行Proxy搜索的函数
        private void StartgProcess(object obj)
        {
            GoogleS gs = obj as GoogleS;
            GetGoogleCookie("ProxyuFunction_GoogleCookie.exe", gs.CookieFilePath);
            gs.pro.StartInfo.FileName = gs.path;                 //设定程序执行路径
            gs.pro.StartInfo.Arguments =gs.flag_searchtype+" "+keyword + " " + gs.PageNum.ToString() + " " + gs.ProcessNum.ToString()+" "+gs.CookieFilePath;    //设定执行参数  
            gs.pro.StartInfo.UseShellExecute = false;                                //关闭Shell的使用  
            gs.pro.StartInfo.RedirectStandardInput = true;                           //重定向标准输入
            gs.pro.StartInfo.RedirectStandardOutput = true;                          //重定向标准输出  
            gs.pro.StartInfo.RedirectStandardError = true;                           //重定向错误输出  
            gs.pro.StartInfo.CreateNoWindow = true;                                  //设置不显示窗口  

            gs.pro.Start();   //进程启动 


            
            gs.pro.WaitForExit(90000);
            if (!gs.pro.HasExited)
                gs.pro.Kill();
            string s = gs.pro.StandardOutput.ReadToEnd();        //从输出流获得结果
            if (s != "couldn't download html files\r\n")
            {
                if (s.Trim() == "need to use proxy")
                {
                    Blocked = true;
                    return;
                }
                if (s.Trim() == "Page is too large")
                {
                    PageIsTooLarge = true;
                    return;
                }
                string[] sArray = s.Split(new char[] { ':', '\n' });


                for (int i = 0; i < sArray.Length / 2; i++)
                {
                    try
                    {
                        int port = int.Parse(sArray[2 * i + 1]);
                        if (port >= 0 && port <= 65535)
                            AddData(new myData(sArray[2 * i].Trim(), sArray[2 * i + 1].Trim(), "Google"));
                    }
                    catch (Exception) { }
                }


            }

            FileInfo file = new FileInfo(gs.ProcessNum + ".xml");
            if (file.Exists)
            {
                try
                {
                    file.Delete(); //删除单个文件
                }
                catch (Exception) { }
            }
            FileInfo file1 = new FileInfo(gs.ProcessNum.ToString());
            if (file1.Exists)
            {
                try
                {
                    file1.Delete();
                }
                catch (Exception) { }
            }
            gs.mre.Set();
        

        }

        //获取Google的Cookie进行下一步的访问
        private void GetGoogleCookie(string path, string cookiefilepath)
        {
            Process pro = new Process();
            pro.StartInfo.FileName = path;                                     //设定程序执行路径
            pro.StartInfo.Arguments = cookiefilepath;    //设定执行参数  
            pro.StartInfo.UseShellExecute = false;                                //关闭Shell的使用  


            pro.StartInfo.CreateNoWindow = true;                                  //设置不显示窗口  
            pro.Start();                                                          //进程启动 

            pro.WaitForExit(3000);                                               //限定进程所运行的时间不超过30s
            if (!pro.HasExited)
                pro.Kill();

        }
       
        //使用URL方式进行Proxy搜索的函数
        public void StarturlProcess(object obj)
        {
            GoogleS gs = obj as GoogleS;
            gs.pro.StartInfo.FileName = gs.path;                                        //设定程序执行路径
            if (gs.path == "ProxyFunction_ITMOPS.exe")
            {
                gs.pro.StartInfo.Arguments = "2 " + gs.ProcessNum.ToString();                              //设定执行参数  
            }
            gs.pro.StartInfo.UseShellExecute = false;                                //关闭Shell的使用  
            gs.pro.StartInfo.RedirectStandardInput = true;                           //重定向标准输入
            gs.pro.StartInfo.RedirectStandardOutput = true;                          //重定向标准输出  
            gs.pro.StartInfo.RedirectStandardError = true;                           //重定向错误输出  
            gs.pro.StartInfo.CreateNoWindow = true;                                  //设置不显示窗口  

            gs.pro.Start();   //进程启动 

            
            gs.pro.WaitForExit(60000);
            if (!gs.pro.HasExited)
            {
                gs.pro.Kill();
            }
            string s = gs.pro.StandardOutput.ReadToEnd();        //从输出流获得结果
            if (s != "couldn't download html files\r\n")
            {
                string[] sArray = s.Split(new char[] { ':', '\n' });
                for (int i = 0; i < sArray.Length / 2; i++)
                {
                    int port = int.Parse(sArray[2 * i + 1]);
                    if (port >= 0 && port <= 65535)
                        AddData(new myData(sArray[2 * i], sArray[2 * i + 1], "URL"));
                }
            }

            FileInfo file = new FileInfo(gs.ProcessNum + ".xml");
            if (file.Exists)
            {
                try
                {
                    file.Delete(); //删除单个文件
                }
                catch (Exception) { }
            }
            gs.mre.Set();
        }

        private void AddData(myData myData)
        {

            this.listview1.Dispatcher.Invoke(
                new Action(
                    delegate
                    {
                        listview1.Items.Add(myData);
                    }
                    )
             );
        }

        //终止搜索按钮
        private void OnCancelButton(object sender, RoutedEventArgs e)
        {
            Thread Cancel = new Thread(new ThreadStart(exec_Cancel));
            Cancel.Start();
        }
        private void exec_Cancel()
        {
            id = page;
            if (Start_Proxy != null && Check_T == null)
            {
                if (Start_Proxy.IsAlive)
                    Start_Proxy.Abort();
                Thread.Sleep(7000);
                System.Diagnostics.Process[][] killprocess = {System.Diagnostics.Process.GetProcessesByName("ProxyFunction_GoogleS"),
                                                             System.Diagnostics.Process.GetProcessesByName("ProxyFunction_SOFTBZ"),
                                                             System.Diagnostics.Process.GetProcessesByName("ProxyFunction_ITMOPS"),
                                                             System.Diagnostics.Process.GetProcessesByName("ProxyFunction_VZLOM"),
                                                             System.Diagnostics.Process.GetProcessesByName("ProxyFunction_PrimeSpeed"),
                                                             System.Diagnostics.Process.GetProcessesByName("ProxyuFunction_GoogleCookie")};
                for (int i = 0; i < killprocess.Length; i++)
                {
                    foreach (System.Diagnostics.Process p in killprocess[i])
                    {
                        if (!p.HasExited)
                        {
                            p.Kill();
                        }
                    }
                }
                
            }
            if (Check_T != null)
            {
                Check_T.Abort();
                CheckButton.Dispatcher.Invoke(new Action(delegate
                {
                    CheckButton.IsEnabled = true;
                }));
            }
            CheckButton.Dispatcher.Invoke(
                new Action(
                    delegate
                    {
                        CheckButton.IsEnabled = true;
                    }));
            startButton.Dispatcher.Invoke(
                new Action(
                    delegate
                    {
                        startButton.IsEnabled = true;
                    }));
            cancelButton.Dispatcher.Invoke(
                new Action(
                    delegate
                    {
                        cancelButton.IsEnabled = false;
                    }));
            if (httpbuchecked)
            {
                for (int i = 0; i < page; i++)
                {
                    FileInfo file = new FileInfo(i + ".xml");
                    FileInfo file1 = new FileInfo(i.ToString());
                    try
                    {
                        if (file.Exists)
                            file.Delete();
                        if (file1.Exists)
                            file1.Delete();
                    }
                    catch (Exception) { }
                }
            }
        }

        private void OnClickHttpConfig(object sender, RoutedEventArgs e)
        {
            
            page = Convert.ToInt32(this.PageCount.Text);
            if (page >= 50)
                page = 50;
            keyword = this.Keyword.Text;
            keyword = keyword.Replace(" ", "+");
            processCount = Convert.ToInt32(this.ProcessCount1.Text);

        }

        private void OnClickUrlConfig(object sender, RoutedEventArgs e)
        {
            checkeds = "";
            if ((Boolean)cb1.IsChecked)
                checkeds += "1";
            if ((Boolean)cb2.IsChecked)
                checkeds += "2";
            if ((Boolean)cb3.IsChecked)
                checkeds += "3";
            if ((Boolean)cb4.IsChecked)
                checkeds += "4";
        }

        public static Boolean CheckProxy(string ip, int port)
        {
            if (port <= 0 || port > 65535)
                return false;
            HttpWebRequest objHttpRequest;
            HttpWebResponse objResponse;
            WebProxy objProxy;
            objHttpRequest = (HttpWebRequest)WebRequest.Create("http://checkip.dyndns.org/"); //设定测试页面
            objHttpRequest.Timeout = 3000; //设定超时时间
            objHttpRequest.AllowAutoRedirect = false;
            
            objProxy = new WebProxy(ip, port);
            objProxy.BypassProxyOnLocal = true;

            objHttpRequest.Proxy = objProxy;
            try
            {
                objResponse = (HttpWebResponse)objHttpRequest.GetResponse();
                Stream receiveStream = objResponse.GetResponseStream();
                StreamReader strReader = new StreamReader(receiveStream);
                string strHTML = strReader.ReadToEnd();
                strReader.Close();
                receiveStream.Close();
                objHttpRequest.Abort();
                objResponse.Close();
                if (strHTML.IndexOf("Current IP Address:") != -1)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false; //无法使用或超时
            }
            
        }

        private void exec_CheckProxy(object obj)
        {
            myData md = obj as myData;
            if (CheckProxy(md.ip, int.Parse(md.port)))
                md.available = "Yes";
            else
                md.available = "No";
            this.listview1.Dispatcher.Invoke(
            new Action(
                delegate
                {
                    if (md.i < listviewcount)
                    {
                        listview1.Items.RemoveAt(md.i);
                        listview1.Items.Insert(md.i, md);
                    }
                    else
                    {
                        listview1.Items.Add(md);
                    }
                }
               )
        );

            if (saveasdb == true)
            {
                
                lock (this)
                {
                    try
                    {
                        
                        Create_DB_File();
                        dbCmd.CommandText = "update ProxyList set Available = '" + md.available + "' where IP = '" + md.ip + "'";
                        dbCmd.ExecuteNonQuery();
                        
                    }
                    catch (SQLiteException) {
                        Create_DB_File();
                        dbCmd.CommandText = "insert into +" + DBTALBE + " values(" + md.ip + "," + md.port + "," + md.type + "," + md.available + ")";
                        dbCmd.ExecuteNonQuery();
                    }
                }
            }
            md.mre.Set();
        }

        private void init_CheckProxy()
        {
            List<ManualResetEvent> manualEvents = new List<ManualResetEvent>();
            if (saveasdb == true)
            {
                dbCmd.CommandText = "SELECT * FROM " + DBTALBE + " WHERE Available = 'Unknown'";
                SQLiteDataReader dataReader = dbCmd.ExecuteReader();
                DataTable dataTable = new DataTable();
                if (dataReader.HasRows)
                {
                    dataTable.Load(dataReader);
                }
                dataReader.Close();
                int j = 0, n = 64;
                int count = dataTable.Rows.Count;
                while (j < count / 64 + 1)
                {
                    if (j == count / 64)
                        n = count;
                    else
                        n = (j + 1) * 64;
                    for (int i = j * 64; i < n; i++)
                    {
                        ManualResetEvent mre = new ManualResetEvent(false);
                        manualEvents.Add(mre);
                        myData md = new myData(dataTable.Rows[i][0].ToString(), dataTable.Rows[i][1].ToString(), dataTable.Rows[i][2].ToString(), dataTable.Rows[i][3].ToString());
                        md.i = i;
                        md.mre = mre;
                        ThreadPool.QueueUserWorkItem(exec_CheckProxy, md);
                    }
                    try
                    {
                        WaitHandle.WaitAll(manualEvents.ToArray());
                        manualEvents.Clear();
                        j++;
                    }
                    catch (Exception)
                    {
                        break;
                    }
                }
            }
            else
            {
                int j = 0, n = 64;
                int count = listview1.Items.Count;
                while (j < count / 64 + 1)
                {
                    if (j == count / 64)
                        n = count;
                    else
                        n = (j + 1) * 64;
                    for (int i = j*64; i < n; i++)
                    {
                        ManualResetEvent mre = new ManualResetEvent(false);
                        manualEvents.Add(mre);
                        myData md = listview1.Items[i] as myData;
                        md.i = i;
                        md.mre = mre;
                        ThreadPool.QueueUserWorkItem(exec_CheckProxy, md);
                    }
                    try
                    {
                        WaitHandle.WaitAll(manualEvents.ToArray());
                        manualEvents.Clear();
                        j++;
                    }
                    catch (Exception)
                    {
                        break;
                    }
                }
            }
           
            this.startButton.Dispatcher.Invoke(
                new Action(
                    delegate
                    {
                        startButton.IsEnabled = true;
                    }
            ));
            this.CheckButton.Dispatcher.Invoke(
                new Action(
                    delegate
                    {
                        CheckButton.IsEnabled = true;
                    }
            ));
            this.cancelButton.Dispatcher.Invoke(
                new Action(
                    delegate
                    {
                        cancelButton.IsEnabled = true;
                    }
            ));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            cancelButton.IsEnabled = false;
            startButton.IsEnabled = false;
            CheckButton.IsEnabled = false;
            cancelButton.IsEnabled = true;
            listviewcount = this.listview1.Items.Count;
            Check_T = new Thread(new ThreadStart(init_CheckProxy));
            Check_T.Start();
        }

        private void export_Button_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new CommonOpenFileDialog();
            dlg.IsFolderPicker = true;
            dlg.EnsurePathExists = true;
            dlg.Multiselect = false;
            string path = "";
            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
            {
                if (dlg.FileName.LastIndexOf("\\") != dlg.FileName.Length - 1)
                    path = dlg.FileName + "\\" + DBNAME;
                else
                    path = dlg.FileName + DBNAME;
                this.storeRoute.Text = path;
                dbSource = path;
            }
            else
            {
                dbSource = DBNAME;
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (Export_Checkbox.IsChecked == true && export_Button.IsEnabled == false)
            {
                export_Button.IsEnabled = true;
                saveasdb = true;
            }
            else if (Export_Checkbox.IsChecked == false && export_Button.IsEnabled == true)
            {
                export_Button.IsEnabled = false;
                saveasdb = false;
            }

        }

        private void Create_DB_File()
        {
            FileInfo db_file = new FileInfo(dbSource);
            if (!db_file.Exists)
                SQLiteConnection.CreateFile(dbSource);

            dbConn = new SQLiteConnection("Data Source=" + dbSource);
            dbConn.Open();
            dbCmd = dbConn.CreateCommand();
            try
            {
                dbCmd.CommandText = "CREATE TABLE " + DBTALBE + "(IP varchar(20),Port varchar(5),Type varchar(10),Available varchar(5),primary key(IP,Port))";
                dbCmd.ExecuteNonQuery();
                dbFile_Available = true;
            }
            catch (Exception) { }
        }

        private void ExportAll_to_Txt(object sender, RoutedEventArgs e)
        {
            if (false == this.startButton.IsEnabled)
                MessageBox.Show("程序还未搜索完毕，不能导出");
            else
            {
                if (this.listview1.Items.Count == 0)
                    MessageBox.Show("不存在搜索结果");
                else
                {
                    CommonSaveFileDialog csfd = new CommonSaveFileDialog();
                    csfd.DefaultExtension = "txt";
                    csfd.Filters.Add(new CommonFileDialogFilter("Text Files", "*.txt"));
                    if (csfd.ShowDialog() == CommonFileDialogResult.Ok)
                    {
                        Thread saveastxt = new Thread(new ParameterizedThreadStart(SaveAsTxt));
                        saveastxt.Start(new export_flag(csfd.FileName,0));
                    }
                }
            }
        }
        private void ExportAvailable_to_txt(object sender, RoutedEventArgs e)
        {
            if (false == this.startButton.IsEnabled)
                MessageBox.Show("程序还未搜索完毕，不能导出");
            else
            {
                if (this.listview1.Items.Count == 0)
                    MessageBox.Show("不存在搜索结果");
                else
                {
                    CommonSaveFileDialog csfd = new CommonSaveFileDialog();
                    csfd.DefaultExtension = "txt";
                    csfd.Filters.Add(new CommonFileDialogFilter("Text Files", "*.txt"));
                    if (csfd.ShowDialog() == CommonFileDialogResult.Ok)
                    {
                        Thread saveastxt = new Thread(new ParameterizedThreadStart(SaveAsTxt));
                        saveastxt.Start(new export_flag(csfd.FileName,1));
                    }
                }
            }
        }
        private void SaveAsTxt(object obj)
        {
            export_flag ef = obj as export_flag;
            myData md;
            string result = "";
            /*this.listview1.Dispatcher.Invoke(
                new Action(
                    delegate
                    {*/
            if (ef.flag == 0)
            {
                for (int i = 0; i < listview1.Items.Count; i++)
                {
                    md = listview1.Items[i] as myData;
                    result += md.ToString();
                }
                File.WriteAllText(ef.path, result);
                
                //         }
                //   )
                //  );
            }
            else
            {
                for (int i = 0; i < listview1.Items.Count; i++)
                {
                    md = listview1.Items[i] as myData;
                    if (md.available == "Yes")
                        result += md.ToString();
                    File.WriteAllText(ef.path, result);
                }
            }
            MessageBox.Show("导出完成！");
        }
    }

}
class export_flag
{
    public string path { get; set; }
    public int flag { set; get; }
    public export_flag(string path, int flag)
    {
        this.path = path;
        this.flag = flag;
    }
}
class myData
{
    public string ip { get; set; }
    public string port { get; set; }
    public string type { get; set; }
    public string available { get; set; }
    public int i { get; set; }
    public ManualResetEvent mre { get; set; }
    public myData(string p1, string p2, string p3, string p4 = "Unknown")
    {
        // TODO: Complete member initialization
        ip = p1;
        port = p2;
        type = p3;
        available = p4;
    }
    public override string ToString()
    {
        return string.Format("{0,-24}",(ip +":"+ port))+"\r\n";
    }
}

class GoogleS
{
    public string KeyWord { get; set; }
    public int PageNum { get; set; }
    public int ProcessNum { get; set; }
    public ManualResetEvent mre { get; set; }
    public string path { get; set; }
    public Process pro { get; set; }
    public int flag_searchtype { get; set; }
    public string CookieFilePath { get; set; }
    public GoogleS(Process pro, int flag_searchtype, string KeyWord, int PageNum, int ProcessNum, string path, string CookieFilePath,ManualResetEvent mre)
    {
        this.CookieFilePath = CookieFilePath;
        this.KeyWord = KeyWord;
        this.PageNum = PageNum;
        this.ProcessNum = ProcessNum;
        this.path = path;
        this.pro = pro;
        this.flag_searchtype = flag_searchtype;
        this.mre = mre;
    }

}

class ListView_Content
{
    public string ip { get; set; }
    public int port { get; set; }
    public ListView_Content(string ip, int port)
    {
        this.ip = ip;
        this.port = port;
    }
}